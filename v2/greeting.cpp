#include <iostream>
#include <string>

using namespace std;

void Greeting(string name){
  cout << "Hello " << name << ", nice to meet you!" << endl;
}

int main(int argc, char const *argv[]) {
  string name;
  cout << "What's your name?" << endl;
  cin >> name;
  Greeting(name);
  return 0;
}
